(function() {

Crafty.c('dude', {

  dude: function() {
    var animSpeed = 20;
    console.log("Animate dude");

    this.requires("SpriteAnimation, Grid")
      .reel("walk", animSpeed, 0, 2, 2);
    return this;

  }

});

})();

(function() {

Crafty.scene('day', function(today) {


    var secondsLeft = 5 * 60 * 1000,
        plantsHarvested = 0,
        shelter = false,
        campfire = false;

    Crafty.e("2D, Canvas, Text")
        .attr({
            w: 100,
            h: 20,
            x: 5,
            y: 5
        })
        .text("Day: " + today)
        .textFont({
            size: '20px',
            weight: 'bold'
        })
        .textColor("#FFFFFF");

    // set plants around the scene

    // set our player in the scene

    Crafty.sprite(16, "images/villager.png", {
        villager: [0, 0]
    });

    var zbase = 2;

    Crafty.c('CharAnims', {
        init: function() {
            var animSpeed = 200
                // setup animations sequences
            this.addComponent("SpriteAnimation, Grid")
                .reel("walk", animSpeed, [
                    [0, 1],
                    [0, 2],
                    [0, 3]
                ]);
            return this;
        }
    });


    Crafty.e('2D, Canvas, villager, CharAnims, Multiway')
        .attr({
            move: {
                left: false,
                right: false,
                up: false,
                down: false
            },
            x: 100,
            y: 100,
            moving: false,
            curAngle: 0
        })
        .bind("Moved", function(from) {
            this.moving = true;
        })
        .bind("EnterFrame", function() {
            // If moving, adjust the proper animation and facing
            if (this.moving) {
                if (!this.isPlaying('walk'))
                    this.animate('walk');
                this.moving = false;
            } else {
                this.pauseAnimation();
            }
        })
        .multiway(2, {
            W: -90,
            S: 90,
            D: 0,
            A: 180
        });

}, function() {
    // destructor
});

})();

(function() {

Crafty.scene('loading', function() {
    console.log("Loading the loading scene");


    var Logo = Crafty.e('2D, Canvas, Color')
        .attr({
            x: 100,
            y: 100,
            w: 300,
            h: 100
        })
        .color("#669900");

    var StartButton = Crafty.e('2D, Canvas, Color, Mouse')
        .attr({
            x: 100,
            y: 250,
            w: 300,
            h: 80
        })
        .color("#FF0000")
        .attach(
            Crafty.e('2D, Canvas, Text')
            .attr({
                x: 150,
                y: 280
            })
            .text('Start')
            .textFont({
                size: '20px',
                weight: 'bold'
            })
        )
        .bind('Click', function(MouseEvent) {
            Crafty.scene('day');
        });




}, function() {
    // destructor
});

})();

(function() {

Crafty.scene('tutorial', function() {

    var tutorialText = "Welcome to the 20 Days Tutorial. This is your chance to click things for pretend points!";

    Crafty.e('2D, DOM, Text')
        .attr({
            x: 100,
            y: 100,
            w: 400
        })
        .text(tutorialText)
        .textFont({
            size: '20px',
            weight: 'bold'
        });

}, function() {
    // destructor
});

})();

(function() {

'use strict';

var Game = {
  // Initialize and start our game
  start: function() {

    Crafty.load(['images/desert-bg.jpg', 'images/villager.png'], function() {

      // Start crafty and set a background color so that we can see it's working
      Crafty.init(800, 600);
      Crafty.background('#CCCCCC url(images/desert-bg.jpg) no-repeat center center');

      var day = 1;

      Crafty.scene('day',day);

    });
  }
};

Game.start();

})();

(function() {

Crafty.debugBar.show();




})();