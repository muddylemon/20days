Crafty.scene('loading', function() {
    console.log("Loading the loading scene");


    var Logo = Crafty.e('2D, Canvas, Color')
        .attr({
            x: 100,
            y: 100,
            w: 300,
            h: 100
        })
        .color("#669900");

    var StartButton = Crafty.e('2D, Canvas, Color, Mouse')
        .attr({
            x: 100,
            y: 250,
            w: 300,
            h: 80
        })
        .color("#FF0000")
        .attach(
            Crafty.e('2D, Canvas, Text')
            .attr({
                x: 150,
                y: 280
            })
            .text('Start')
            .textFont({
                size: '20px',
                weight: 'bold'
            })
        )
        .bind('Click', function(MouseEvent) {
            Crafty.scene('day');
        });




}, function() {
    // destructor
});