Crafty.scene('tutorial', function() {

    var tutorialText = "Welcome to the 20 Days Tutorial. This is your chance to click things for pretend points!";

    Crafty.e('2D, DOM, Text')
        .attr({
            x: 100,
            y: 100,
            w: 400
        })
        .text(tutorialText)
        .textFont({
            size: '20px',
            weight: 'bold'
        });

}, function() {
    // destructor
});