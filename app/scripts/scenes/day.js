Crafty.scene('day', function(today) {


    var secondsLeft = 5 * 60 * 1000,
        plantsHarvested = 0,
        shelter = false,
        campfire = false;

    Crafty.e("2D, Canvas, Text")
        .attr({
            w: 100,
            h: 20,
            x: 5,
            y: 5
        })
        .text("Day: " + today)
        .textFont({
            size: '20px',
            weight: 'bold'
        })
        .textColor("#FFFFFF");

    // set plants around the scene

    // set our player in the scene

    Crafty.sprite(16, "images/villager.png", {
        villager: [0, 0]
    });

    var zbase = 2;

    Crafty.c('CharAnims', {
        init: function() {
            var animSpeed = 200
                // setup animations sequences
            this.addComponent("SpriteAnimation, Grid")
                .reel("walk", animSpeed, [
                    [0, 1],
                    [0, 2],
                    [0, 3]
                ]);
            return this;
        }
    });


    Crafty.e('2D, Canvas, villager, CharAnims, Multiway')
        .attr({
            move: {
                left: false,
                right: false,
                up: false,
                down: false
            },
            x: 100,
            y: 100,
            moving: false,
            curAngle: 0
        })
        .bind("Moved", function(from) {
            this.moving = true;
        })
        .bind("EnterFrame", function() {
            // If moving, adjust the proper animation and facing
            if (this.moving) {
                if (!this.isPlaying('walk'))
                    this.animate('walk');
                this.moving = false;
            } else {
                this.pauseAnimation();
            }
        })
        .multiway(2, {
            W: -90,
            S: 90,
            D: 0,
            A: 180
        });

}, function() {
    // destructor
});