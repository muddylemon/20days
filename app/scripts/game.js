'use strict';

var Game = {
  // Initialize and start our game
  start: function() {

    Crafty.load(['images/desert-bg.jpg', 'images/villager.png'], function() {

      // Start crafty and set a background color so that we can see it's working
      Crafty.init(800, 600);
      Crafty.background('#CCCCCC url(images/desert-bg.jpg) no-repeat center center');

      var day = 1;

      Crafty.scene('day',day);

    });
  }
};

Game.start();